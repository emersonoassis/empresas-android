package com.example.empresa

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.empresa.login.domain.usecase.LoginUsecase
import com.example.empresa.login.ui.LoginFragment
import com.example.empresa.login.ui.LoginViewModel
import com.example.empresa.util.inscribeUI
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    private val loginViewModel: LoginViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        inscribeUI(R.id.screen_content, createFragment())
    }

    private fun createFragment(): Fragment {
        return LoginFragment.newInstance(loginViewModel)
    }
}
