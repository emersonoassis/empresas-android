package com.example.empresa.di

import android.content.Context
import com.example.empresa.R
import com.example.empresa.login.data.LoginDataSource
import com.example.empresa.login.data.LoginDataSourceImpl
import com.example.empresa.login.domain.usecase.LoginUsecase
import com.example.empresa.login.domain.usecase.LoginUsecaseImpl
import com.example.empresa.login.ui.LoginViewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val dbModule = module {

    single {
        Retrofit
            .Builder()
            .baseUrl(get<Context>().getString(R.string.ioasys_host))
            .addConverterFactory(
                GsonConverterFactory.create()
            )
            .build()

    }
}

val uiModule = module {

    fun provideLoginDataSource() : LoginDataSource = LoginDataSourceImpl(get())
    fun provideLoginUsecase(): LoginUsecase  = LoginUsecaseImpl(provideLoginDataSource())

    single { LoginViewModel(context = get(), loginUsecase =  provideLoginUsecase()) }
}