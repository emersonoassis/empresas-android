package com.example.empresa

import android.app.Application
import com.example.empresa.di.dbModule
import com.example.empresa.di.uiModule
import org.koin.android.ext.android.startKoin

class EmpresaApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(dbModule, uiModule))
    }
}