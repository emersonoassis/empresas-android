package com.example.empresa.login.domain.usecase

import android.content.Context
import com.example.empresa.login.data.LoginDataSource
import com.example.empresa.login.data.LoginDataSourceImpl
import com.example.empresa.login.domain.model.ResponseApi
import com.example.empresa.login.domain.model.UserAuthentication
import retrofit2.Response

class LoginUsecaseImpl(val repository: LoginDataSource) : LoginUsecase {

    override suspend fun login(userAuthentication: UserAuthentication, context: Context): Response<ResponseApi> {
        return repository.signIn(userAuthentication, context)
    }
}