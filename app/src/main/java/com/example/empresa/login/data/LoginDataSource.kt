package com.example.empresa.login.data

import android.content.Context
import com.example.empresa.login.domain.model.ResponseApi
import com.example.empresa.login.domain.model.UserAuthentication
import retrofit2.Response

interface LoginDataSource {
    suspend fun signIn(userAuthentication: UserAuthentication, context: Context) : Response<ResponseApi>
}