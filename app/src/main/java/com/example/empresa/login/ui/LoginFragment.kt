package com.example.empresa.login.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.empresa.databinding.LoginFragmentBinding


class LoginFragment : Fragment() {

    private lateinit var loginViewModel: LoginViewModel

    companion object {
        fun newInstance(viewModel: LoginViewModel) : LoginFragment {
            val fragment = LoginFragment()
            fragment.loginViewModel = viewModel

            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding : LoginFragmentBinding = LoginFragmentBinding
            .inflate(inflater, container, false)
        binding.loginViewModel = loginViewModel

        return binding.root

    }

}
