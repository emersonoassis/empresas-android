package com.example.empresa.login.data

import android.content.Context
import com.example.empresa.login.domain.model.ResponseApi
import com.example.empresa.login.domain.model.UserAuthentication
import retrofit2.Response
import retrofit2.Retrofit

class LoginDataSourceImpl(val retrofit: Retrofit) : LoginDataSource {

    override suspend fun signIn(userAuthentication: UserAuthentication, context: Context): Response<ResponseApi> {
        val service = retrofit.create(LoginApi::class.java)
        return service.signIn(userAuthentication)
    }
}