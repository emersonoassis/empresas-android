package com.example.empresa.login.domain.model

data class UserAuthentication(val email: String,
                              val password: String)