package com.example.empresa.login.domain.usecase

import android.content.Context
import com.example.empresa.login.domain.model.ResponseApi
import com.example.empresa.login.domain.model.UserAuthentication
import retrofit2.Response

interface LoginUsecase {
    suspend fun login(userAuthentication: UserAuthentication, context: Context): Response<ResponseApi>
}