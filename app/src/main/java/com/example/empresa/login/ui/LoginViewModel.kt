package com.example.empresa.login.ui

import android.content.Context
import android.view.View
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.example.empresa.R
import com.example.empresa.login.domain.model.UserAuthentication
import com.example.empresa.login.domain.usecase.LoginUsecase
import com.example.empresa.util.showSnackBar
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LoginViewModel(val context: Context, val loginUsecase: LoginUsecase) : ViewModel() {

    val email: ObservableField<String> = ObservableField()
    val password: ObservableField<String> = ObservableField()
    val loadingVisibility: ObservableBoolean = ObservableBoolean(false)

    fun onClickLogin(view: View) {

        if (hasFieldEmpty()) {
            view.showSnackBar(context.getString(R.string.empty_fields_message))
        } else {
            login(view)
        }
    }

    private fun hasFieldEmpty(): Boolean = email.get().isNullOrEmpty() || password.get().isNullOrEmpty()

    private fun login (view: View) {

        CoroutineScope(Dispatchers.IO).launch {

            loadingVisibility.set(true)

            val response = loginUsecase.login(
                UserAuthentication(
                    email.get().toString(),
                    password.get().toString()
                ), context
            )

            withContext(Dispatchers.Main) {

                loadingVisibility.set(false)

                if (response.isSuccessful) {

                    Snackbar.make(view, response.body().toString(), Snackbar.LENGTH_LONG).show()
                } else {
                    Snackbar.make(view, context.getString(R.string.no_authenticated_user_message), Snackbar.LENGTH_LONG).show()
                }

            }
        }
    }

}
