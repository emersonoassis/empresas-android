package com.example.empresa.login.data

import com.example.empresa.login.domain.model.ResponseApi
import com.example.empresa.login.domain.model.UserAuthentication
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginApi {

    @POST("users/auth/sign_in")
    suspend fun signIn(@Body userAuthentication: UserAuthentication): Response<ResponseApi>
}