package com.example.empresa.login.domain.model

data class ResponseApi (
    val errors: ArrayList<String> = arrayListOf(),
    val success: Boolean
)